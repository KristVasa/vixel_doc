var searchData=
[
  ['on_5faction_5fabout_5ftriggered_26',['on_action_About_triggered',['../classInterface.html#a24f83e98b4f2c2cef516522990b021f5',1,'Interface']]],
  ['on_5faction_5fban_5flist_5ftriggered_27',['on_action_Ban_List_triggered',['../classInterface.html#a76db10a2a9fbb1b38c29ace06a91f5b5',1,'Interface']]],
  ['on_5faction_5fexit_5ftriggered_28',['on_action_Exit_triggered',['../classInterface.html#a15e6e14b21b423e520c41c8738899cff',1,'Interface']]],
  ['on_5faction_5ffit_5fto_5fwindow_5ftriggered_29',['on_action_Fit_To_Window_triggered',['../classInterface.html#ab84a875e12fe244f4e32fd46855eeb10',1,'Interface']]],
  ['on_5faction_5fnormal_5fsize_5ftriggered_30',['on_action_Normal_Size_triggered',['../classInterface.html#a23843ebf73982fdc48f61ad6db7e17bb',1,'Interface']]],
  ['on_5faction_5fopen_5ftriggered_31',['on_action_Open_triggered',['../classInterface.html#ac80aa53d4d601ae825b2e1624933cfb4',1,'Interface']]],
  ['on_5faction_5fzoom_5fin_5ftriggered_32',['on_action_Zoom_In_triggered',['../classInterface.html#a76ab323a1732e1fecba48f653a925e34',1,'Interface']]],
  ['on_5faction_5fzoom_5fout_5ftriggered_33',['on_action_Zoom_Out_triggered',['../classInterface.html#add6f8b83bda7c8a051ebaf074bd29f9e',1,'Interface']]],
  ['on_5factionsave_5fas_5ftriggered_34',['on_actionSave_As_triggered',['../classInterface.html#afe2edc8e74191295aa431a0fdebc5575',1,'Interface']]],
  ['openimage_35',['openImage',['../classImageProc.html#acb63c8287f525502f97e16d236838c37',1,'ImageProc']]]
];
